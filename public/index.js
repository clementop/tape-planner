var $ = {
    new: function (node) { return document.createElement(node); },
    q: function(query, node) { return (node || document).querySelectorAll(query); },
    q1: function(query, node) { return (node || document).querySelector(query); },
    id: function(id) { return document.getElementById(id); },
};

var OPTIONS = {
    side: 47 * 60, // seconds per side (48:23 timed on rap tape)
    lead: 5, // seconds silence on side start
    tail: 5, // seconds silence on side end
    gaps: 0, // seconds silence between songs
};

var mixtapes = JSON.parse(localStorage.getItem("mixtapes") || "{}");
var currentMixtape = localStorage.getItem("currentMixtape") || "Mixtape 1";
var mixtape = mixtapes[currentMixtape] || {};
if (mixtape.tracks == null) {
    mixtape.tracks = [];
}
save();

function durationAsString (durationInSeconds) {
    var hours = Math.floor(durationInSeconds / 60 / 60);
    var minutes = Math.floor(durationInSeconds / 60) - (60 * hours);
    var seconds = durationInSeconds % 60;

    var formatted = "";
    if (hours > 0) {
        formatted += hours + ":";
    }
    if (hours > 0 && minutes < 10) {
        formatted += "0" + minutes + ":";
    } else {
        formatted += minutes + ":";
    }
    if (seconds < 10) {
        formatted += "0" + seconds;
    } else {
        formatted += seconds;
    }

    return formatted;
}

function save () {
    mixtapes[currentMixtape] = mixtape;
    localStorage.setItem("mixtapes", JSON.stringify(mixtapes));
    localStorage.setItem("currentMixtape", currentMixtape);
}

function rename (newName) {
    delete mixtapes[currentMixtape];
    currentMixtape = newName;
    mixtapes[currentMixtape] = mixtape;
    save();
}

function drawProgress (percentage, track, title, striped) {
    var $progress = getTemplateCopy("progress-bar");
    $progress.style.width = "" + percentage + "%";
    $progress.innerText = track;
    $progress.setAttribute("title", title);
    if (striped) {
        // $progress.classList.add("progress-bar-striped");
        $progress.classList.add("bg-info");
    }
    new bootstrap.Tooltip($progress);
    return $progress;
}

function onClickRename () {
    var $input = $.new("input");
    $input.setAttribute("type", "text");
    $input.classList.add("form-control", "form-control-sm", "me-3");
    $input.value = currentMixtape;
    $.id("mixtape-name").replaceWith($input);

    $input.focus();
    $input.select();

    var refreshTitle = function () {
        var $name = $.new("h3");
        $name.id = "mixtape-name";
        $name.classList.add("mb-0");
        $name.innerText = currentMixtape;
        $input.replaceWith($name);
        listen($name, "click", onClickRename);
        refreshLayout();
    };

    listen($input, "keyup", function (e) {
        if (e.key === "Enter") {
            rename(e.target.value);
        }

        if (e.key === "Enter" || e.key == "Escape") {
            off($input, "blur", refreshTitle);
            refreshTitle();
        }
    });
    listen($input, "blur", refreshTitle);
}

function refreshLayout () {
    var side1 = [];
    var side2 = [];
    var side1Silence = OPTIONS.side;
    var side2Silence = OPTIONS.side;
    var sideDuration = OPTIONS.lead + OPTIONS.tail;
    var onSide2 = false;
    var trackList = [];
    var striped = false;
    for (var i = 0; i < mixtape.tracks.length; i++) {
        var track = mixtape.tracks[i];
        var $track = drawProgress(track[0] / OPTIONS.side * 100, i+1, track[1], striped);
        striped = !striped;
        if (sideDuration + track[0] > OPTIONS.side) {
            if (onSide2) {
                break;
            }
            onSide2 = true;
            sideDuration = OPTIONS.lead + OPTIONS.tail;
        }
        sideDuration += track[0];
        if (onSide2) {
            side2Silence = OPTIONS.side - sideDuration;
            side2.push($track);
        } else {
            side1Silence = OPTIONS.side - sideDuration;
            side1.push($track);
        }
    }

    for (var i = 0; i < mixtape.tracks.length; i++) {
        var track = mixtape.tracks[i];
        trackList.push(drawTrack(track[2], track[3], track[1], i));
    }

    $.id("progress-side-a").innerHTML = null;
    for (var i = 0; i < side1.length; i++) {
        $.id("progress-side-a").append(side1[i]);
    }
    $.id("progress-side-b").innerHTML = null;
    for (var i = 0; i < side2.length; i++) {
        $.id("progress-side-b").append(side2[i]);
    }

    $.id("side-1-silence").innerText = durationAsString(side1Silence);
    $.id("side-2-silence").innerText = durationAsString(side2Silence);

    $.id("mixtape-name").innerText = currentMixtape;
    listen($.id("mixtape-name"), "click", onClickRename);

    var $trackList = $.id("track-list")
    $trackList.innerHTML = null;
    for (var i = 0; i < trackList.length; i++) {
        $trackList.append(trackList[i]);
    }
    if (trackList.length == 0) {
        var $noTracks = $.new("li");
        $noTracks.classList.add("list-group-item");
        $noTracks.innerText = "No tracks added";
        $trackList.append($noTracks);
    }

    new Sortable($trackList, {
        animation: 150,
        ghostClass: "list-group-item-info",
        handle: ".drag-icon",
        onEnd: function (e) {
            if (e.oldIndex != e.newIndex) {
                mixtape.tracks.splice(e.newIndex, 0, mixtape.tracks.splice(e.oldIndex, 1)[0]);
                save();
                refreshLayout();
            }
        }
    });

    var $mixtapeList = $.id("mixtape-list");

    $.id("mixtape-list").innerHTML = null;
    var names = Object.keys(mixtapes).sort();
    for (var i = 0; i < names.length; i++) {
        var $button = $.new("button");
        $button.classList.add("dropdown-item");
        $button.innerText = names[i];
        if (names[i] === currentMixtape) {
            $button.classList.add("text-primary");
            // $button.classList.add("active");
        }
        $button.setAttribute("type", "button");
        listen($button, "click", onChangeMixtape);

        $.id("mixtape-list").append($button);
    }

    var $hr = $.new("hr");
    $hr.classList.add("dropdown-divider");

    var $removeButton = $.new("button");
    $removeButton.classList.add("dropdown-item", "text-danger");
    $removeButton.innerText = "Delete";
    listen($removeButton, "click", onRemoveMixtape);


    $mixtapeList.append($hr);
    $mixtapeList.append($removeButton);

    listen($.id("new-mixtape"), "click", onNewMixtape);
}

function onChangeMixtape (e) {
    setMixtape(e.target.innerText);
}

function setMixtape (next) {
    currentMixtape = next;
    mixtape = mixtapes[currentMixtape];
    save();
    refreshLayout();
}

function onNewMixtape () {
    currentMixtape = "Mixtape " + (Object.keys(mixtapes).length + 1);
    mixtape = { tracks: []};
    save();
    refreshLayout();
}

function onRemoveMixtape () {
    delete mixtapes[currentMixtape];
    var rem = Object.keys(mixtapes).sort();
    if (rem.length == 0) {
        onNewMixtape();
    } else {
        setMixtape(rem[0]);
    }
}

function getTemplateCopy (className) {
    return $.q1(".templates ." + className).cloneNode(true);
}

function drawTrack (min, sec, title, index) {

    var $track = getTemplateCopy("track");
    $.q1(".track-duration", $track).innerText = durationAsString((min*60) + sec);
    $.q1(".track-title", $track).innerText = title;

    var $remove = $.q1(".remove-track", $track);
    $remove.setAttribute("track-index", index);
    listen($remove, "click", onRemoveTrack);

    return $track;

}

function onAddTrack (event) {
    var $min = $.q1("form.add-track input.min");
    var $sec = $.q1("form.add-track input.sec");
    var $title = $.q1("form.add-track input.title");

    var min = parseInt($min.value);
    var sec = parseInt($sec.value);
    var title = $title.value || "Track " + (mixtape.tracks.length + 1);

    mixtape.tracks.push([(min*60)+sec, title, min, sec]);
    save();

    $min.value = null;
    $sec.value = null;
    $title.value = null;

    $min.focus();

    refreshLayout();

    event.preventDefault();
}

function onRemoveTrack (event) {
    var trackIndex = parseInt(event.target.getAttribute("track-index"));
    mixtape.tracks.splice(trackIndex, 1);
    save();
    refreshLayout();
}

function listen (target, event, callback) {
    target.addEventListener(event, callback);
}

function off (target, event, callback) {
    target.removeEventListener(event, callback);
}

function ready (fn) {
    if (document.readyState != 'loading'){
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

ready(function () {
    $.q1("form.add-track input.min").focus();
    listen($.q1("form.add-track"), "submit", onAddTrack);
    refreshLayout();
});
